package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type WatchFile struct {
	filepattern    string
	matcherpattern *regexp.Regexp
}

func printSlice(s []WatchFile) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func logAndExit(err error) {
	if err != nil {
		log.Fatalln(err)
		os.Exit(1)
	}
}

func parseFlags() []WatchFile {

	var watchFiles []WatchFile

	printSlice(watchFiles)

	argumentMatcher, err := regexp.Compile("^-filenames=(.*)$")
	logAndExit(err)

	for _, element := range os.Args[1:] {
		fmt.Println("Parsing element: " + element)

		if argumentMatcher.MatchString(element) {
			if strings.Contains(element, ",") {
				splittedElement := strings.Split(argumentMatcher.FindStringSubmatch(element)[1], ",")
				matcherpattern, err := regexp.Compile(splittedElement[1])
				logAndExit(err)
				watchFiles = append(watchFiles, WatchFile{filepattern: splittedElement[0], matcherpattern: matcherpattern})
			} else {
				watchFiles = append(watchFiles, WatchFile{filepattern: argumentMatcher.FindStringSubmatch(element)[1]})
			}
		}
	}

	printSlice(watchFiles)
	return watchFiles
}

func lookForUpdates(watchFiles []WatchFile) {

	for _, watchFile := range watchFiles {
		fmt.Println("Evaluating file wildcard expression: " + watchFile.filepattern)
		files, err := filepath.Glob(watchFile.filepattern)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(files) // contains a list of all files in the current directory
	}
}

func main() {
	watchFiles := parseFlags()

	lookForUpdates(watchFiles)

}
